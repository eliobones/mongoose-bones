"use strict"
const { PermitLevels } = require("../auth/permits")
const settings = require("../settings")

// Deprecated. Use `engage.Permit` or store in list, e.g. engage.ItemList.itemListElement[Permit]
module.exports = (schema, options) => {
  schema.add({
    permits: {
      type: Tyoe,
      default: settings.permits,
    },
  })
}
