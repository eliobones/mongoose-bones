# mongoose-bones Credits

## Core, thanks!

**mongoose-bones** was originally cloned from **mongoose-api-generator**. Though **mongoose-bones** is now quite different, I found it to be the perfect starting point, given their similar aims.

- [cktang88/mongoose-api-generator](https://github.com/cktang88/mongoose-api-generator)

Thanks, cktang88.

- [expressjs](https://expressjs.com/)
- [mongoosejs](http://mongoosejs.com/)
- [mochajs](https://mochajs.org/)
- [chaijs](http://www.chaijs.com/)
- [mollusc](https://www.npmjs.com/package/mollusc)

## These were helpful

- [codementor/olatundegaruba/nodejs-restful-apis-in-10-minutes](https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd)
- [djamseed/building-restful-apis-with-express-and-mongodb](https://www.djamseed.com/2016/03/30/building-restful-apis-with-express-and-mongodb/)
- [bitnami/tutorials/deploy-rest-api-nodejs-mongodb-charts](https://docs.bitnami.com/tutorials/deploy-rest-api-nodejs-mongodb-charts/)
- <https://medium.com/hackernoon/node-js-development-with-skaffold-file-sync-99c687b906d5>
- <https://www.sentinelstand.com/article/docker-with-node-in-development-and-production>

## Testing considerations

- [medium/nongaap/writing-mongodb-mongoose-unit-tests-using-mocha-chai](https://medium.com/nongaap/beginners-guide-to-writing-mongodb-mongoose-unit-tests-using-mocha-chai-ab5bdf3d3b1d)
- [scotch/tutorials/test-a-node-restful-api-with-mocha-and-chai](https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai)
- [youtube/tdd-express-mocha](https://www.youtube.com/watch?v=BwNMUVzo3vs)
- [lucasfcosta/testing-nodejs-apis](https://lucasfcosta.com/2017/04/06/Testing-NodeJS-APIs.html)
- [github/DigiPie/mocha-chai-mongoose-config](https://github.com/DigiPie/mocha-chai-mongoose/tree/master/server/src/config)
- [stackoverflow/questions/running-mocha-setup-before-each-suite-rather-than-before-each-test](https://stackoverflow.com/questions/26107027/running-mocha-setup-before-each-suite-rather-than-before-each-test)

## These might be helpful

- [github/sriracha/express and mongoose admin backend](https://github.com/hdngr/sriracha)
- [npmjs/mangostana/mongoose documents relationships](https://www.npmjs.com/package/mangostana)
- [hapi/joi schema definitions](https://hapi.dev/module/joi/)
- <https://www.npmjs.com/package/mongoose-relationship>
- <https://docs.mongodb.com/manual/tutorial/model-tree-structures-with-materialized-paths/>
- <https://www.npmjs.com/package/mongoose-mpath>

## Artwork

- [wikimedia/banded_mongoose_skeleton](https://commons.wikimedia.org/wiki/File:Banded_mongoose_Skeleton.jpg)
