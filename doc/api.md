# mongoose-bones API

## Authentication

## `signup`

- POST `/auth/:engage/signup` Signup a Thing of type `:engage`, e.g. "Thing", "MedicalCondition", "Vehicle".

  - Required Params: `engage`
  - Required Fields: `username`, `password`

```shell
curl -X POST http://localhost:5000/auth/MedicalCondition/signup -d name="FleshName" -d username="flesh" -d password="letmein"
```

Response:

```json
{
  "_id": "5f4c3f8171342431cb677148",
  "permits": {
    "get": "ANON",
    "create": "ANON",
    "update": "ANON",
    "delete": "ANON"
  },
  "list": [],
  "name": "FleshName",
  "username": "flesh",
  "created": "2020-08-31T00:08:33.534Z",
  "thing": "Thing"
}
```

## `login`

- POST `/auth/login/` Login with username and password.

  Required Fields: `username`, `password`

```shell
curl -X POST http://localhost:5000/auth/login -d username="flesh" -d password="letmein"
```

Response:

```json
{
  "_id": "5f4c3f8171342431cb677148",
  "name": "FleshName",
  "success": true,
  "username": "flesh",
  "token": "Bearer eyJhbGciOiJIUzI1NiIsInR...."
}
```

Using Envvars:

```shell
set AUTH_RES (curl -X POST http://localhost:5000/auth/login -d username="flesh" -d password="letmein")
set AUTH_RES (curl -X POST http://localhost:5000/auth/Thing/login -d username="flesh" -d password="letmein")
set BEARER (string match -r '(?<=token\"\:\").*(?=\"\})' $AUTH_RES)
set ENGAGED (string match -r '(?<=_id\"\:\").*(?=\"\})' $AUTH_RES)
```

## `logout`

- GET `/auth/logout/` Expire the jwt token.

## CRUD

## `engage`

- GET `/:engage/:_id/` View the _engaged_ Thing.

  - Required Params: `engage`, `_id`

```shell
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/"
```

Response:

```json
{
  "permits": {
    "get": "ANON",
    "create": "ANON",
    "update": "ANON",
    "delete": "ANON"
  },
  "list": [],
  "_id": "5f4c3f8171342431cb677148",
  "name": "FleshName",
  "username": "flesh",
  "created": "2020-08-31T00:08:33.534Z",
  "thing": "Thing",
  "__v": 0
}
```

## `update`

- PATCH `/:engage/:_id/` Update the _engaged_ Thing.

  - Required Params: `engage`, `_id`

```shell
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/"
```

Response:

```json
{
  "permits": {
    "get": "ANON",
    "create": "ANON",
    "update": "ANON",
    "delete": "ANON"
  },
  "list": [],
  "_id": "5f4c3f8171342431cb677148",
  "name": "FleshName",
  "username": "flesh",
  "created": "2020-08-31T00:08:33.534Z",
  "thing": "Thing",
  "__v": 0
}
```

## `delete`

- DELETE `/:engage/:_id/` Delete the _engaged_ Thing.

  - Required Params: `engage`, `_id`

```shell
curl -X DELETE -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/"
```

Response:

```json
{
  "permits": {
    "get": "ANON",
    "create": "ANON",
    "update": "ANON",
    "delete": "ANON"
  }
}
```

## `create`

- POST `/:engage/:_id/:T/` Create a new Thing of type `:T` and adds it to the _engaged_ Thing's list.

  - Required Params: `engage`, `_id`, `T`

```shell
curl -X POST -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/MedicalCondition/" -d name="New MedicalCondition in List For $ENGAGED"
```

Response:

```json
{
  "_id": "5f4c4b9671342431cb67714a",
  "permits": {
    "get": "ANON",
    "create": "ANON",
    "update": "ANON",
    "delete": "ANON"
  },
  "list": [],
  "name": "New MedicalCondition in List For $ENGAGED",
  "created": "2020-08-31T01:00:06.391Z",
  "createdBy": "$AUTHENTICATED",
  "god": "$ENGAGED",
  "thing": "MedicalCondition",
  "__v": 0
}
```

## `list`

- GET `/:engage/:_id/list/` View _engaged_ Thing's list.

  - Required Params: `engage`, `_id`

```shell
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/list/"
```

Response:

```json
[
  {
    "_id": "5f4c4ace71342431cb677149",
    "name": "New Thing in List For $ENGAGED",
    "thing": "T"
  },
  {
    "_id": "5f4c4b9671342431cb67714a",
    "name": "New MedicalCondition in List For $ENGAGED",
    "thing": "MedicalCondition"
  }
]
```

Settings:

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  slim: {
    disambiguatingDescription: 1,
    name: 1,
  },
}
```

## `listof`

- GET `/:engage/:_id/listof/:T/` View _engaged_ Thing's list of type `:T`.

  - Required Params: `engage`, `_id`

```shell
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/list/MedicalCondition/"
```

Response:

```json
[
  {
    "_id": "5f4c4b9671342431cb67714a",
    "name": "New MedicalCondition in List For 5f4c3f8171342431cb677148",
    "thing": "MedicalCondition"
  }
]
```

Settings:

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  slim: {
    disambiguatingDescription: 1,
    name: 1,
  },
}
```

## Meta

## `schema`

GET `/schema/:engage/` Get the Meta Schema of type `:engage`.

- Required Params: `engage`

```shell
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/schema/Thing/"
```

Response:

```json
{
  "disambiguatingDescription": {
    "type": "Text"
  },
  "name": {
    "type": "Text"
  }
}
```

Settings:

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  slim: {
    disambiguatingDescription: 1,
    name: 1,
  },
}
```
