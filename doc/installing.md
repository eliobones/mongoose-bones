# Installing mongoose-bones

- [Prerequisites](/eliobones/mongoose-bones/prerequisites.html)

## Setup

```shell
npm i -g @elioway/generator-bones
git clone https://gitlab.com/eliobones/mongoose-bones.git bones-myapp
cd bones-myapp
```
