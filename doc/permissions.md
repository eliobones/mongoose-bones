# Permissions

Permissions enable you to implement granular restrictions on who can perform which action on the Thing belonging to the`:_id` parameter.

In **mongoose-bones** individual Things have the permissions, not routes. Routes have authentication, not permissions. Endpoints will only respond if the Thing belonging to the `$BEARER` token, has permission to `get`/`create`/`update`/`delete` the Thing `$ENGAGED`.

## Turning on the Permissions Adon

- Ensure `ThingSchema.plugin(require("./adon/permits"))` is uncommented in `mongoose-bones/bones/ThingModel.js`

This adds on a `permits` field to every Thing, giving each a map of permission options and actions.

## The options are:

- `GOD` Token `$BEARER` must be `god` of `$ENGAGED` Thing to perform the action on `$ENGAGED` Thing.
- `LISTED` Token `$BEARER` must be `listed` by `$ENGAGED` Thing to perform the action on `$ENGAGED` Thing.
- `AUTH` Any Token `$BEARER` has permission to perform the action on `$ENGAGED` Thing.
- `ANON` Token Not Required: Anon users have permission to perform the action on `$ENGAGED` Thing.

These are available in the const object `PermitLevels` in `eliobones/mongoose-bones/bones/auth/permits.js`:

```javascript
// eliobones/mongoose-bones/bones/auth/permits.js
const PermitLevels = {
  GOD: "GOD",
  LISTED: "LISTED",
  AUTH: "AUTH",
  ANON: "ANON",
}
```

## The actions are:

- `get` Permission to see `$ENGAGED` Thing.
- `create` Permission create a new Thing and add it to `$ENGAGED` Thing's list.
- `update` Permission to update `$ENGAGED` Thing.
- `delete` Permission to delete `$ENGAGED` Thing.

`BasePermits` demostrates a sensible default in `eliobones/mongoose-bones/bones/auth/permits.js` you should choose a default permission which will cover most cases. These will, by default, be used for any new Things created.

Example:

```javascript
// eliobones/mongoose-bones/bones/auth/permits.js
const BasePermits = {
  get: PermitLevels.AUTH,
  create: PermitLevels.LISTED,
  update: PermitLevels.GOD,
  delete: PermitLevels.GOD,
}
```

## Default permissions

Each Thing starts with a default for `permits` which is applied from the site [Settings Documentation][settings.html]

However, this default is applied to each and every Thing individually, and can be changed on Thing by Thing basis.

## Editing permissions

Permissions should be added to your client's Thing editing forms so Users can change them for specific Things and override the defaults. You can also use **curl** to do the same thing.

For instance, to make a Thing you own completely private to everyone except you:

```shell
curl -X PATCH http://localhost:5000/Thing/5f4ae2eb52699e1b2a5b11d6/ \
  -d '{ "permits": {"get": "GOD", "create": "GOD", "update": "GOD", "delete": "GOD" } }' \
  -H "Content-Type: application/json"
```
