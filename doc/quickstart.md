# Quickstart mongoose-bones

- [mongoose-bones Prerequisites](/eliobones/mongoose-bones/prerequisites.html)
- [Installing mongoose-bones](/eliobones/mongoose-bones/installing.html)

## Nutshell

Start it:

```shell
minikube start
docker-compose up
```

## curling

Get started by curling some requests, just to make sure it's working.

```shell
curl -X POST http://localhost:5000/auth/Thing/signup -d name=elio -d username=elioadmin -d password=letmein
set AUTH_RES (curl -X POST http://localhost:5000/auth/Thing/login -d username=elioadmin -d password=letmein)
set BEARER (string match -r '(?<=token\"\:\").*(?=\"\})' $AUTH_RES)
set ENGAGED (string match -r '(?<=_id\"\:\").*(?=\"\})' $AUTH_RES)

curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/"
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/list/"
curl -X GET -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/listof/SportEvent/"

curl -X DELETE -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/"

curl -X POST -H "Authorization: $BEARER" "http://localhost:5000/$ENGAGED/" -d name="I'm a new Thing in the $ENGAGED Thing's list"

curl -X PATCH -H "Authorization: $BEARER" "http://localhost:5000/Thing/$ENGAGED/" -d name="newName"
```

## Permissions

Each record in the database contains it's own `permits` Map allowing or forbidding certain actions to certain authentication levels. See [Permissions Documentation](/eliobones/mongoose-bones/permissions.html) for more information.

### Curl

```shell
curl -X PATCH http://localhost:5000/Thing/5f4ae2eb52699e1b2a5b11d6/ \
  -d '{ "permits": {"get": "GOD", "create": "GOD", "update": "GOD", "delete": "GOD" } }' \
  -H "Content-Type: application/json"
```

### Settings

- `guard`: The middleware which protects the express server.
- `sanitizers`:
- `slim`

See [Settings Documentation](/eliobones/mongoose-bones/settings.html)

## Endpoints

This is what your client app will need to know.

Available Paremeters:

1. `:engage` is a <https://schema.org> "Thing" type. Examples are `/:engage/Thing/` `/:engage/Person/` `/:engage/LocalBusiness/`:
2. `:_id` is the unique Mongoose Document id of the _engaged_ Thing.
3. `:T` is a <https://schema.org> "Thing" type. Used to create a new Thing of this type, and add it to the list of the _engaged_ Thing.

Here is a list of endpoints mongoose-bones makes available to your clients:

- POST `/auth/:engage/signup` Signup a Thing of type `:engage`, e.g. "Thing", "MedicalCondition", "Vehicle".
- POST `/auth/login/` Login with username and password.
- GET `/auth/logout/` Expire the jwt token.
- GET `/:engage/:_id/` View the _engaged_ Thing.
- PATCH `/:engage/:_id/` Update the _engaged_ Thing.
- DELETE `/:engage/:_id/` Delete the _engaged_ Thing.
- POST `/:engage/:_id/:T/` Create a new Thing of type `:T` and add it to the _engaged_ Thing's list.
- GET `/:engage/:_id/list/` View _engaged_ Thing's list.
- GET `/:engage/:_id/list/:T/` View _engaged_ Thing's list of type `:T`.
- GET `/schema/:engage/` Get the Meta Schema of type `:engage`.

See [Api Documentation](/eliobones/mongoose-bones/api.html)
