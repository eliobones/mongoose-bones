# Writing a Passport Strategy

```javascript
const passportCustom = require("passport-custom")

passport.use(
  "strategy-name",
  new passportCustom.Strategy((req, callback) => {
    // Do your custom user finding logic here, or set to false based on req object
    callback(null, user)
  })
)

module.exports = T => {
  passport.use(
    "unguarded",
    new passportCustom.Strategy((req, callback) => {
      T.findOne({ username: "god" })
        .then(user => {
          console.log(user, "<= user")
          return callback(null, user)
        })
        .catch(err => {
          console.log("Login Error:", err)
          return callback(err)
        })
    })
  )
  return passport.authenticate("unguarded", { session: false })
}
```
