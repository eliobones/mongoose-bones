# mongoose-bones Authentication

As a default, we use [passportjs](http://www.passportjs.org/) combined with [passport-jwt](https://github/passport-jwt), but **passport** Authentication is implemented using standard **expressjs** Middleware, and you can easily change it.

## Use the UnGuarded Authentication

`UnGuarded` is a useful Authentication Strategy for development - where the authentication is implemented later. It allows for completely ANON usage, even though **eliobones** would not normally allow for it. Your client will be able to access the Restful API without needed to sign the user in.

It works by using a hard-coded Mongoose Thing ID to act as "proxy signedin" for any request; populating the session "User".

To use `UnGuarded`, change the `guard` setting in `bones/settings.js` and add the Mongoose siteId of the "proxy" Thing:

```javascript
// bones/settings.js
const unGuarded = require("./auth/unGuarded")
module.exports = {
  guard: unGuarded,
  siteId: '5f4b845241f12f5b1a8524a0',
  ...
}
```

## Add custom passportjs Middleware Authentication

[passport-custom](https://github/passport-custom) is installed, and this makes it easy to create an entirely custom Authentication Middleware plugin, e.g.:

```javascript
//eliobones/mongoose-bones/bones/auth/dummy.js
const passportCustom = require("passport-custom")
module.exports = () => {
  passport.use(
    "dummy",
    new passportCustom.Strategy((req, callback) => {
      user = { name: "I am a Dummy" }
      callback(null, user)
    })
  )
  return passport.authenticate("dummy", { session: false })
}
```

To use this, change the `guard` setting in `bones/settings.js`:

```javascript
// bones/settings.js
const dummy = require("./auth/dummy")
module.exports = {
  guard: dummy,
  ...
}
```

Here's one that could be used to log someone in using a specific parameter in the URL.

```javascript
//eliobones/mongoose-bones/bones/auth/urlId.js
module.exports = () => {
  passport.use(
    "urlid",
    new passportCustom.Strategy((req, callback) => {
      T.findOneById(req.params.userid)
        .then(user => {
          return callback(null, user)
        })
        .catch(err => {
          return callback(err)
        })
    })
  )
  return passport.authenticate("urlid", { session: false })
}
```

To use this, change the `guard` setting in `bones/settings.js`:

```javascript
// bones/settings.js
const urlId = require("./auth/urlId")
module.exports = {
  guard: urlId,
  ...
}
```

Check <http://www.passportjs.org/> which has examples on using other types of off-the-shelf Authentication methods, including OAuth, and has details on writing a custom `Strategy` if **passport-custom** doesn't cover your requirements.

## Other Authentication Methods

Any ExpressJs compatible Middleware will work with **mongoose-bones**.
