# mongoose-bones Settings

There are some settings you can and should change in the `mongoose-bones/bones/settings.js` file.

## `guard`

See [Authentication Documentation](/eliobones/mongoose-bones/authentication.html) for more info:

Protect all endpoints with passport-awt Authentication.

```javascript
// `mongoose-bones/bones/settings.js`
const jwtAuthGuard = require("./auth/jwtAuthGuard")
module.exports = {
  guard: jwtAuthGuard,
}
```

Allow access to all endpoints without any Authentication. All users share the site's default account given by `siteId` setting (see below).

```javascript
// `mongoose-bones/bones/settings.js`
const unGuarded = require("./auth/unGuarded")
module.exports = {
  guard: unGuarded,
  siteId: "5f4b845241f12f5b1a8524a0",
}
```

## `permits`

See [Permissions Documentation](/eliobones/mongoose-bones/permissions.html) for more info:

`BasePermits` work great with token auth:

```javascript
// `mongoose-bones/bones/settings.js`
const { BasePermits } = require("./auth/permits")
module.exports = {
  guard: jwtAuthGuard,
  permits: BasePermits,
}
```

`AnonPermits`, anyone can change anything.

```javascript
// `mongoose-bones/bones/settings.js`
const { AnonPermits } = require("./auth/permits")
module.exports = {
  guard: unGuarded,
  siteId: "5f4b845241f12f5b1a8524a0",
  permits: AnonPermits,
}
```

Custom default permits:

```javascript
// `mongoose-bones/bones/settings.js`
const { PermitLevels } = require("./auth/permits")
module.exports = {
  permits: {
    get: PermitLevels.ANON,
    create: PermitLevels.GOD,
    update: PermitLevels.LISTED,
    delete: PermitLevels.GOD,
  },
}
```

## `sanitizers`

Functions for named querystrings you might want to sanitize.

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  sanitizers: {
    queryStringName: queryString => {
      return queryString || {}
    },
    anotherQueryStringName: queryString => {
      return resolveToDefaultOrValue(queryString)
    },
  },
}
```

## `sideId`

The Mongoose Object ID of the Thing representing the site or grandparent of all Things.

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  siteId: "5f4b845241f12f5b1a8524a0",
}
```

## `slim`

A dictionary of selected Thing field names (valued 1) which slims up the Thing lists.

Include only these named fields:

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  slim: {
    disambiguatingDescription: 1,
    name: 1,
  },
}
```

Any fields except these named fields:

```javascript
// `mongoose-bones/bones/settings.js`
module.exports = {
  slim: {
    additionalType: 0,
    alternateName: 0,
    description: 0,
  },
}
```

## `bones`

The connection URL to **MongoDb**. This is best handled in an .env file:

```javascript
const { MONGODB_URL, BONES, BONES_DEPTH, EXTRA_PRIMITIVE } = process.env
// `mongoose-bones/bones/settings.js`
module.exports = {
  bones: BONES.split(",").filter(b => b),
  ...
}
```

## `depth`

The connection URL to **MongoDb**.

This can be handled in the `.env` file:

```javascript
const { MONGODB_URL, BONES, BONES_DEPTH, EXTRA_PRIMITIVE } = process.env
// `mongoose-bones/bones/settings.js`
module.exports = {
  depth: BONES_DEPTH || 0,
  ...
}
```

## `extraPrimitive`

Thing Types you expect **thing** to collect as Primitive. For instance, <https://schema.org> `ImageObject` is a complex Type you might wish to resolve to a single field type in your MVC, like with Django's `ImageField` type. Adding `ImageObject` to the list of `extraPrimitive` ensures any applicable fields show this Type.

This can be handled in the `.env` file:

```javascript
const { EXTRA_PRIMITIVE } = process.env
// `mongoose-bones/bones/settings.js`
module.exports = {
  extraPrimitive: EXTRA_PRIMITIVE.split(",").filter(b => b),
  ...
}
```

## `mongoDbUrl`

The connection URL to **MongoDb**. This is best handled in an .env file:

```javascript
const { MONGODB_URL } = process.env
// `mongoose-bones/bones/settings.js`
module.exports = {
  mongoDbUrl: MONGODB_URL,
  ...
}
```
