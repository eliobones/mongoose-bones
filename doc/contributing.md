# mongoose-bones Contributing

## Repo

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
git clone https://gitlab.com/elioway/eliobones.git
git clone https://gitlab.com/elioway/eliothing.git
cd elioangels
git clone https://gitlab.com/elioangels/daemon.git
git clone https://gitlab.com/elioangels/pinocchio.git
cd eliothing
git clone https://gitlab.com/eliothing/thing.git
git clone https://gitlab.com/eliothing/mongoose-thing.git
git clone https://gitlab.com/eliothing/liar-thing.git
cd eliobones
git clone https://gitlab.com/eliobones/mongoose-bones.git
cd eliobones/mongoose-bones
```

## Contributing

- [Quickstart daemon](/elioangels/daemon/quickstart.html)

```shell
# wake it up?
docker version --format "v{{.Client.Version}}"
minikube start
```

- [Cheat docker-compose](/elioangels/jeeves/cheat-docker-compose.html)
- [mongo-daemon Recipe](/eliobones/mongo-daemon/recipe.html)

```shell
cd path/to/elioapp
docker-compose up
```

### `npm test`

### `npm run prettier`

### `npm run dev`

### TODOS

1. The `bones` folder is from the **bones** module. Extract the universal stuff.
