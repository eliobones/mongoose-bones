<aside>
  <dl>
  <dd>Him thus intent Ithuriel with his spear</dd>
  <dd>Touched lightly; for no falshood can endure</dd>
  <dd>Touch of celestial temper</dd>
</dl>
</aside>

Mongo version of endpoints, **the elioWay**.

<div><a href="api.html">
  <button>API</button>
</a>
  <a href="authentication.html">
  <button>Authentication</button>
</a>
  <a href="permissions.html">
  <button>Permissions</button>
</a>
  <a href="settings.html">
  <button>Settings</button>
</a></div>

# Requires

- [eliothing/thing](/eliothing/thing)
- [eliothing/mongoose-thing](/eliothing/mongoose-thing)
