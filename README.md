![](https://elioway.gitlab.io/eliobones/elio-mongoose-bones-logo.png)

# mongoose-bones

![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

[ExpressJs](expressjs.com) and [MongooseJs](mongoosejs.com) backed REST API built on <https://schema.org> Types, **the elioWay**.

- [mongoose-bones Documentation](https://elioway.gitlab.io/eliobones/mongoose-bones/)

## Installing

```shell
git clone https://gitlab.com/eliobones/mongoose-bones.git my-mongoose-bones-api
```

- [Installing mongoose-bones](https://elioway.gitlab.io/eliobones/mongoose-bones/installing.html)

## Seeing is Believing

```shell
minikube start
docker-compose up
```

```shell
# See it!
curl -X POST http://localhost:5000/schema/MedicalCondition/

curl -X POST http://localhost:5000/auth/Thing/signup \
  -d name="FleshName" \
  -d username="flesh" \
  -d password="letmein"

set AUTH_RES (curl -X POST http://localhost:5000/auth/Thing/login \
  -d username="flesh" \
  -d password="letmein" \
)
set BEARER (string match -r '(?<=token\"\:\").*(?=\"\})' $AUTH_RES)
set ENGAGED (string match -r '(?<=_id\"\:\").*(?=\"\})' $AUTH_RES)

# Get Thing by ID
curl -X GET -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/

# Create a New Thing to the list belonging to ID
curl -X POST -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/MedicalCondition/ \
  -d name="MedicalCondition Created for $ENGAGED's List"

# Delete Thing by ID
curl -X DELETE -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/

# Update a Thing by ID
curl -X PATCH -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/ \
  -d name="newName for $ENGAGED"

# Get List of Things belonging to ID
curl -X GET -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/list/

# Get a filtered List of MedicalCondition Type Things belonging to ID
curl -X GET -H "Authorization: $BEARER" http://localhost:5000/Thing/$ENGAGED/list/MedicalCondition/
```

## Nutshell

### `npm start`

### `npm test`

### `npm run prettier`

- [mongoose-bones Quickstart](https://elioway.gitlab.io/eliobones/mongoose-bones/quickstart.html)
- [mongoose-bones Credits](https://elioway.gitlab.io/eliobones/mongoose-bones/credits.html)
- [mongoose-bones Issues](https://elioway.gitlab.io/eliobones/mongoose-bones/issues.html)

![](https://elioway.gitlab.io/eliobones/mongoose-bones/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
