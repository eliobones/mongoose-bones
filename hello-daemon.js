"use strict"
const express = require("express")
const mongoose = require("mongoose")
const database = require("@elioway/bones/bones/database")

const app = express()

let connected = false

database
  .dbconnect("mongodb://mongoose-bones-mongo:27017/mongoose-bones-daemon")
  .on("error", err => console.log({ app: "err" }, err))
  .on("connected", stream => {
    connected = true
  })

console.log(`The MongoDb-Daemon is waking up 27018:27017...`)

app.get("/", (req, res) => {
  let readyState = connected
    ? "is awake and hungry."
    : "doesn't want to play. Come back later. "
  res.send(`The MongoDb-Daemon ${readyState}`)
})

app.listen(3020, () =>
  console.log(`curl http://localhost:${3020}/ to poke the MongoDb-Daemon.`)
)

// require("dotenv").config()
// const { MONGODB_URL, PORT } = process.env
